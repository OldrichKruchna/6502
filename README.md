# 6502

This project aggregates all the snippets used to build the 8-bit computer around the 6502 microprocessor. It's based on the fantastic Ben Eater's course https://eater.net/6502